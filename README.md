# Performance pipeline template

## TL;DR
В корне проекта создайте файл .gitlab-ci.yml
```yaml
# Подключить шаблонный пайплайн
include:
  - project: 'tinkoffperfworkshop/part-1/perftest-pipeline-template'
    ref: 'v0.1.0'
    file: '.performance-test-pipeline.yml'
# Переопределить переменные из шаблона или добавить новые
variables:
  SIMULATION: "Stability"
```

## Содержание

* [Общая информация](#общая-информация)
* [Установка](#установка)
* [Настройка](#настройка)

## Общая информация

Проект содержит шаблон пайплайна для проведения тестов производительности.

Шаги:
 * test - запуск теста
 * report - генерация отчета
 
Шаги запускаются вручную, в артефактах report будет лежать отчет gatling.

## Установка

Для подключения пайплайна в свой репозиторий с тестом gatling необходимо в корне проекта создать файл .gitlab-ci.yml
```yaml
include:
  - project: 'tinkoffperfworkshop/part-1/perftest-pipeline-template'
    ref: 'v0.1.0'
    file: '.performance-test-pipeline.yml'
```

## Настройка

### Запуск теста
На шаге test выполняется команда:
```shell script
sbt "gatling:testOnly $ORGANISATION.$SERVICE_NAME.$SIMULATION"
```

Для запуска теста необходимо переопределить переменные $ORGANISATION, $SERVICE_NAME, $SIMULATION в блоке variables:
```yaml
variables:
  ORGANISATION: "ru.tinkoff.load"
  SERVICE_NAME: "myservice"
  SIMULATION: "Debug"
```
По результату будет сформирован путь до файла симуляции:
ru.tinkoff.load.myservice.Debug

### Параметризация теста
Можно передавать любые переменные определенные в проекте gatling, сделать это можно в блоке variables:
```yaml
variables:
  intensity: "10"
  rampDuration: "1m"
  stageDuration: "5m"
```

### Настройка интеграций gatling
По-умолчанию определены следующие значения:
```yaml
variables:
  CONSOLE_LOGGING: "ON" # включить логирование в stdout
  GRAPHITE_HOST: "influxdb" # host influxdb для записи метрик gatling
  GRAPHITE_PORT: "2003" # port influxdb для записи метрик gatling
  INFLUX_PREFIX: "experiment" # имя проекта в influxdb
```
Их можно переопределить в блоке variables.

### Настройка зависимостей агента
По-умолчанию определены следующие значения:
```yaml
variables:
  JAVA_VERSION: "11.0.8"
  SBT_VERSION: "1.3.13"
  SCALA_VERSION: "2.12.12"
```
Их можно переопределить в блоке variables.
Доступные версии образов можно уточнить на [DockerHub](https://hub.docker.com/r/hseeberger/scala-sbt/)
